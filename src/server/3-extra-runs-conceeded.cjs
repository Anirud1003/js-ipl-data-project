const fs = require("fs");
const csv = require("csvtojson")
const path = require("path")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const csvFilePath1 = path.join(__dirname,"../data/deliveries.csv")
const jsonFilePath = path.join(__dirname,"../public/output/3-extra-runs-conceeded.json")

function extraRunsPerTeam(callback) {
    csv().fromFile(csvFilePath).then((matchesData) => {
        csv().fromFile(csvFilePath1).then((deliveriesData)=>{
                const ids = matchesData.filter(match => 
                    Number(match.season) ===2016)

                    const extraRuns = deliveriesData
                    .filter((delivery) => (delivery.match_id) in ids)
                    .reduce((acc, curr) => {
                        if (acc.hasOwnProperty(curr.bowling_team)) {
                          acc[curr.bowling_team] += +curr.extra_runs
                        } else {
                          acc[curr.bowling_team] = +curr.extra_runs
                        }               
                        return acc
                      },{})
        
            fs.writeFileSync(jsonFilePath,JSON.stringify(extraRuns,null,2))
            callback(null,extraRuns)
        })
        
    })
}
module.exports = extraRunsPerTeam
