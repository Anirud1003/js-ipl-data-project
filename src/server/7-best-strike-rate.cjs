const csv = require("csvtojson")
const path = require("path")
const fs = require("fs")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const csvFilePath1 = path.join(__dirname,"../data/deliveries.csv")
const jsonFilePath = path.join(__dirname,"../public/output/7-best-strike-rate.json")

function strikeRate(callback) {
    csv().fromFile(csvFilePath).then((matachesData) => {
        csv().fromFile(csvFilePath1).then((deliveriesData) => {
            let final = {}
            const batsman = "DA Warner"
            let array = deliveriesData.filter(item => {
                return item.batsman === "DA Warner"
            })
    
            const ids = matachesData.map(elements => 
                elements[elements.id] = elements.season)
    
            array.reduce((acc,curr) => {
                if (acc[ids[curr.match_id]]) {
                    const balls = acc[ids[curr.match_id]].balls + 1
                    const runs = acc[ids[curr.match_id]].runs + Number(curr.batsman_runs)
                    const strikerate = ((runs / (balls)) * 100).toFixed(1)
                    acc[ids[curr.match_id]] = { balls, runs, strikerate }
                } else {
                    const balls = 1
                    const runs =Number(curr.batsman_runs)
                    const strikerate = ((runs / (balls)) * 100).toFixed(1)
                    acc[ids[curr.match_id]] = { balls, runs, strikerate }
                }
                return final[batsman] = acc
            })
            fs.writeFileSync(jsonFilePath,JSON.stringify(final,null,2))
            callback(null,final)
        })
        
    })
}
module.exports = strikeRate