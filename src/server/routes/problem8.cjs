const express = require("express")
const router =  express.Router()
const playerDismissed = require("../8-one-player-dismissed-other-player.cjs")

router.get('/', (request,response) => {
    playerDismissed((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router