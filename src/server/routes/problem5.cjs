const express = require("express")
const router =  express.Router()
const tossAndMatch = require("../5-team-won-toss-won-match.cjs")

router.get('/', (request,response) => {
    tossAndMatch((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router