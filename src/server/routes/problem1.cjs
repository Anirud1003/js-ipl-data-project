const express = require("express")
const router =  express.Router()
const matchesPerYear = require("../1-matches-per-year.cjs")

router.get('/', (request,response) => {
    matchesPerYear((error,data)=>{
        if(error) {
            console.log(error)
        } else {
            response.send(data)
        }
    })

})

module.exports = router