const express = require("express")
const router =  express.Router()
const bowlersEconomy = require("../4-economical-bowler-2015.cjs")

router.get('/', (request,response) => {
    bowlersEconomy((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router