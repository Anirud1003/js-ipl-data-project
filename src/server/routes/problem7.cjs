const express = require("express")
const router =  express.Router()
const strikeRate = require("../7-best-strike-rate.cjs")

router.get('/', (request,response) => {
    strikeRate((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router