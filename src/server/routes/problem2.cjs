const express = require("express")
const router =  express.Router()
const winsPerTeam = require("../2-matches-won-per-team-per-year.cjs")

router.get('/', (request,response) => {
    winsPerTeam((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router