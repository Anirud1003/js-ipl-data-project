const express = require("express")
const router =  express.Router()
const bestEconomySuperOver= require("../9-best-economy-in-super-over.cjs")

router.get('/', (request,response) => {
    bestEconomySuperOver((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router