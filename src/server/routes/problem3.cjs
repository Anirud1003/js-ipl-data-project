const express = require("express")
const router =  express.Router()
const extraRunsPerTeam = require("../3-extra-runs-conceeded.cjs")

router.get('/', (request,response) => {
    extraRunsPerTeam((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router