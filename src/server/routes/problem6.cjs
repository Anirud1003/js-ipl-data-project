const express = require("express")
const router =  express.Router()
const manOfTheMatch= require("../6-won-player-of-the-match-most.cjs")

router.get('/', (request,response) => {
    manOfTheMatch((error,data) => {
        if (error) {
            response.send(error)
        } else {
            response.send(data)
        }
    })
})

module.exports = router