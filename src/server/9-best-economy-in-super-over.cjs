const fs = require("fs");
const csv = require("csvtojson")
const path = require("path");

const csvFilePath = path.join(__dirname,"../data/deliveries.csv")
const jsonFilePath = path.join(__dirname,"../public/output/9-best-economy-in-super-over.json")

function superOverEconomicalBowler(callback) {
    csv().fromFile(csvFilePath).then((deliveriesData) => {
      
      const rows = deliveriesData.filter(element => {
        if (element.is_super_over > 0) {
          return element
        }
      })
      

     let bowlers = rows.reduce((acc,element) => {
          if (acc[element.bowler]) {
              const bowled = element.ball <= 6 ? acc[element.bowler].balls + 1 : acc[element.bowler].balls
              const runs = acc[element.bowler].runs + Number(element.total_runs)
              const overCalc = (Math.floor(bowled/6))
              const economy = (runs / (overCalc) + (((bowled - (overCalc * 6)) / 10))).toFixed(1)
              
              acc[element.bowler] = { 
                  "runs": runs,
                  "balls":bowled,
                  "economy":economy
              }
          } else {
              const runs = Number(element.total_runs)
              const bowled = 1

              acc[element.bowler] = {
                  "runs":runs,
                  "balls":bowled
              }
          }
          return acc
      },{})

      let entries = (Object.entries(Object.values(bowlers)))

      let min = Number.POSITIVE_INFINITY
      const index = entries.map(element => {
          if (min > Number(element[1].economy)) {
              min = element[1].economy
          }
          return element
      })

      let result=Object.keys(bowlers).reduce((acc,element) => {
          if ((bowlers[element].economy) === min) {
              acc[element] = min
          }
          return acc
      },{})

      fs.writeFileSync(jsonFilePath,JSON.stringify(result,null,2))
      callback(null,result)
  })
}
module.exports = superOverEconomicalBowler 

