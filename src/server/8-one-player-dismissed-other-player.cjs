const csv = require("csvtojson")
const path = require("path")
const fs = require("fs")

const csvFilePath = path.join(__dirname,"../data/deliveries.csv")
const jsonFilePath = path.join(__dirname,"../public/output/8-one-player-dismissed-other-player.json")

function playerDismissed(callback) {
    csv().fromFile(csvFilePath).then((deliveriesData) => {
        let dismissalRows =  deliveriesData.filter(wicket => 
            wicket.player_dismissed && wicket.dismissal_kind !== "runout"
        )

        let ans = dismissalRows.reduce((final,wicket1) => {
            if (final[wicket1.batsman]) {
                if (final[wicket1.batsman][wicket1.bowler]) {
                    final[wicket1.batsman][wicket1.bowler]++
                } else {
                    final[wicket1.batsman][wicket1.bowler] = 1
                }
                
            } else {
                final[wicket1.batsman] = {}
                final[wicket1.batsman][wicket1.bowler] = 1
            }
            return final
        },{})

        let max = -1
        let batsman,bowler

        Object.keys(ans).reduce((dismissal,element)=>{
            dismissal.push((Object.entries(ans[element])).sort((a, b) => b[1] - a[1])[0]);
            dismissal.map((ball) => {
                if (ball[1] > max) {
                    batsman = element
                    bowler = ball[0]
                    max = ball[1]
                }
                return ball
            })
            return dismissal
        },[])
        let result = (`${batsman} has been dismissed by ${bowler} ${max} times .`)
        fs.writeFileSync(jsonFilePath,JSON.stringify(result,null,2))
        callback(null,result)
    })
}
module.exports = playerDismissed
