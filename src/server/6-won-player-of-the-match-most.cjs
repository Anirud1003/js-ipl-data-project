const csv = require("csvtojson")
const path = require("path")
const fs = require("fs")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const jsonFilePath = path.join(__dirname,"../public/output/6-won-player-of-the-match-most.json")

function manOfTheMatch(callback) {
    csv().fromFile(csvFilePath).then((matchesData) => {
        let players = matchesData.map((match) => [match.season, match.player_of_match])
        .reduce((accu, curr) => {
          [season, manOfMatch] = [curr[0], curr[1]];
    
          if (accu.hasOwnProperty(season)) {
            if (accu[season].hasOwnProperty(manOfMatch)) {
              accu[season][manOfMatch]++
            } else {
              accu[season][manOfMatch] = 1
            }
          } else {
            accu[season] = {[manOfMatch]: 1}
          }
          return accu
        }, {})
    
        const res = (Object.entries(players)).map(element => Object.entries(element[1]).sort((a,b) => b[1] -a[1]))
        let result = {}
        const keys = Object.keys(players)

        res.map((element,index) => {
            result[keys[index]] = element[0][0]
            return element
        })
        fs.writeFileSync(jsonFilePath,JSON.stringify(result,null,2))
        callback(null,result)
    })
}
module.exports = manOfTheMatch
