const path =  require("path")
const csv = require("csvtojson")
const fs = require("fs")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const jsonFilePath = path.join(__dirname,"../public/output/1-matches-per-year.json")

function matchesPerYear(callback) {
  
  csv().fromFile(csvFilePath).then((matchesData) => {
    let result = matchesData.reduce( (accu, curr) => {
      let match = curr.season
      if (accu.hasOwnProperty(match)) {
        accu[match] += 1
      } else {
        accu[match] = 1
      }
      return accu
    }, {})
    
    fs.writeFileSync(jsonFilePath,JSON.stringify(result,null,2))
    callback(null,result)
    
  }) 
}


module.exports = matchesPerYear