const csv = require("csvtojson")
const path = require("path")
const fs = require("fs")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const jsonFilePath = path.join(__dirname,"../public/output/2-matches-won-per-team-per-year.json")

function winsPerTeam(callback) {
    csv().fromFile(csvFilePath).then((matchesData) => {
        let result = matchesData.reduce((accu, match) => {
            if (accu.hasOwnProperty(match.season)) {
                let internalAccu = accu[match.season]
                if(internalAccu.hasOwnProperty(match.winner)) {
                    internalAccu[match.winner]++
                } else {
                    internalAccu[match.winner] = 1
                }
            } else {
                accu[match.season] = {
                    [match.winner] : 1
                }
            }    
            return accu
        }, {})

        fs.writeFileSync(jsonFilePath,JSON.stringify(result,null,2))
        callback(null,result)
    })
}
module.exports = winsPerTeam