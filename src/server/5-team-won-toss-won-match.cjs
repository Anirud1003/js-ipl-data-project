const fs = require("fs")
const csv = require("csvtojson")
const path = require("path")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const jsonFilePath = path.join(__dirname,"../public/output/5-team-won-toss-won-match.json")

function tossAndMatch(callback) {
    csv().fromFile(csvFilePath).then((matchesData) => {
        const result = matchesData.reduce((accumulator, current) => {
            if (current.toss_winner === current.winner) {
              if (accumulator.hasOwnProperty(current.winner)) {
                accumulator[current.winner]++;
              } else {
                accumulator[current.winner] = 1
              }
            }
        
            return accumulator
          }, {})
        
        fs.writeFileSync(jsonFilePath,JSON.stringify(result,null,2))
        callback(null,result)
    })
} 
module.exports = tossAndMatch
    