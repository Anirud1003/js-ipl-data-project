const path =  require("path")
const csv = require("csvtojson")
const fs = require("fs")

const csvFilePath = path.join(__dirname,"../data/matches.csv")
const csvFilePath1 = path.join(__dirname,"../data/deliveries.csv")
const jsonOutput = path.join(__dirname, "../public/output/4-economical-bowler-2015.json")

function economicalBowlers(callback){

    csv().fromFile(csvFilePath).then((matchesData) => {
        csv().fromFile(csvFilePath1).then((deliveriesData) => {
            const matchIds = []
            const match =[]
            bowlers ={}
            matchesData.map(element => {
                if (Number(element.season) === 2015) {
                    matchIds.push(element.id)
                }
                return element
            })
            
            
            matchIds.map(item => {
                (deliveriesData.filter(elements => {
                    if (elements.match_id === item) {
                        match.push(elements)
                    }
                }))
                return item
            })
            
            match.map(element => {
                if (bowlers[element.bowler]) {
                    const bowled = element.ball <= 6 ? bowlers[element.bowler].bowled + 1 :bowlers[element.bowler].bowled
                    const runs = bowlers[element.bowler].runs + Number (element.total_runs)
                    bowlers[element.bowler] = { bowled, runs }
                } else {
                    const bowled = 1
                    const runs = Number(element.total_runs)
                    bowlers[element.bowler] = { bowled, runs}
                }
                return element
            })
            

            let answer = Object.keys(bowlers).reduce((arr,keys) => {
                const oversBowled = (bowlers[keys].bowled / 6).toFixed(1)
                const bowler = bowlers[keys]
                const economy = (Number[bowler.runs] / oversBowled).toFixed(1)
                bowlers[keys] = { ...bowler, overs : oversBowled, economy : economy}
                arr.push({bowler : keys, ...bowlers[keys]})
                return arr
            },[])

            answer.sort((bowler1,bowler2) => {
                if (Number(bowler1.economy) < (bowler2.economy)) {
                    return -1
                }
                if (Number(bowler1.economy) > (bowler2.economy)) {
                    return 1
                }
                return 0
            })
            let topEconomy =(answer.splice(0,10))

            fs.writeFileSync(jsonOutput,JSON.stringify(topEconomy,null,2))
            callback(null,topEconomy)
        })
    })
}

module.exports = economicalBowlers