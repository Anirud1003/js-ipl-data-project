const path = require("path")
const express =require("express")
const app = express()

const problem1Router = require('../server/routes/problem1.cjs')
const problem2Router = require('../server/routes/problem2.cjs')
const problem3Router = require('../server/routes/problem3.cjs')
const problem4Router = require('../server/routes/problem4.cjs')
const problem5Router = require('../server/routes/problem5.cjs')
const problem6Router = require('../server/routes/problem6.cjs')
const problem7Router = require('../server/routes/problem7.cjs')
const problem8Router = require('../server/routes/problem8.cjs')
const problem9Router = require('../server/routes/problem9.cjs')

let port = process.env.PORT || 2000

app.use(express.static(__dirname));

app.get('/',(request,response) => {
    response.sendFile(path.join(__dirname,"./index.html"))
})

app.use('/1',problem1Router)
app.use('/2',problem2Router)
app.use('/3',problem3Router)
app.use('/4',problem4Router)
app.use('/5',problem5Router)
app.use('/6',problem6Router)
app.use('/7',problem7Router)
app.use('/8',problem8Router)
app.use('/9',problem9Router)

app.listen(port,() => {
    console.log(port)
})
